package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class NotesViewModel: ViewModel() {
    var notesList = MutableLiveData<List<Note>>()

    fun getNotesRepository() {
        notesList.value = NotesRepository.notesList
    }

    fun saveNoteToRepository(note: Note) {
        NotesRepository.notesList.add(note)
    }

    fun deleteNoteFromRepository(note: Note) {
        NotesRepository.notesList.remove(note)
    }

    fun updateNote(updatedNote: Note, position: Int) {
        var oldNote: Note = NotesRepository.notesList[position]
        oldNote.noteTitle = updatedNote.noteTitle
        oldNote.noteDescription = updatedNote.noteDescription
        oldNote.noteDate = updatedNote.noteDate
    }

    fun getNoteFromRepository(position: Int): Note {
        return NotesRepository.notesList[position]
    }
}